You have been provided with 2 ﬁles: books.xml and donuts.json that are to be used to create a web page that is used to display both raw and formatted XML and JSON data using Node.js with Express.js. 
In all cases, the XML or JSON data must be read and returned to a template where it is displayed/rendered. While creating this page, you must complete the following tasks:
-----------------------------------------------------------------------------------------------------------------------------------
Task1: Create a donuts.xml ﬁle that is the equivalent of the donuts.json ﬁle. 
-----------------------------------------------------------------------------------------------------------------------------------
Task2: Create a books.json ﬁle that is the equivalent of the books.xml ﬁle.
-----------------------------------------------------------------------------------------------------------------------------------
Task3: Add functionality to your page by creating eight links (four concerning the Donuts and four concerning the books) 
-----------------------------------------------------------------------------------------------------------------------------------
For each set of data, the four links will be as follows: 
-----------------------------------------------------------------------------------------------------------------------------------
a) “Raw XML” – When this link is clicked, it should display the raw XML of the books or donuts ﬁle. The XML should be displayed inside of a "pre" tag and will be missing the actual XML tags. 
-----------------------------------------------------------------------------------------------------------------------------------
b) “Raw JSON” – The same as the “Raw XML” link, except that, in this case, the raw JSON data of the books or donuts ﬁle will be displayed 
-----------------------------------------------------------------------------------------------------------------------------------
c) “Displayed XML” – When this link is clicked, it should display the XML of the books or donuts ﬁle in a well-formatted way, perhaps using a table or other method of display. The table should be centered and use the Bootstrap classes table-hover, table-striped, and table-bordered. 
-----------------------------------------------------------------------------------------------------------------------------------
d) “Displayed JSON” – This task is identical to “Displayed XML”, except it must be completed using the JSON data as a data source.
-----------------------------------------------------------------------------------------------------------------------------------


Instructions for running this assignment (cmd: node app.js) using Node.js with Express.js

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:

1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_XML_JSON
. Type this command: node app.js
. Then now you can go to your browser with domain: http://localhost:3000/

2. Using PowerShell in Windows system or Command Prompt
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_XML_JSON
. Type this command: node app.js
. Then now you can go to your browser with domain: http://localhost:3000/