var express = require('express');
var router = express.Router();

var fs      = require('fs');
var xml2js  = require('xml2js');
var parser  = new xml2js.Parser();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'PHUONG NGUYEN, Assignment #10' });
});


router.get('/rawbooksxml', function(req, res, next) {
    var booksxml = __dirname + "/../public/xml/books.xml";
    fs.readFile(booksxml, "utf-8", function (error, text) {
        if (error) 
            throw error;
        else
            res.render('raw', { title: 'Raw Books XML ', content:  text });
    });
});


router.get('/rawbooksjson', function(req, res, next) {
    var booksjson = __dirname + "/../public/json/books.json";
    fs.readFile(booksjson, "utf-8", function (error, text) {
        if (error)
            throw error;
        else 
            res.render('raw', { title: 'Raw Books JSON ', content:  text });
    });
});


router.get('/booksxml', function(req, res, next) {
    var booksxml = __dirname + "/../public/xml/books.xml";
    fs.readFile(booksxml, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {
            parser.parseString(text, function (err, result) {
                var books = result['catalog']['book'];
                console.log(books[0]);
                res.render('booksxml', { title: 'Books XML ', books:  books });
            });
        }
    });
});


router.get('/booksjson', function(req, res, next) {
    var jsonFile = __dirname + "/../public/json/books.json";
    fs.readFile(jsonFile, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {
            var books = JSON.parse(text);
            books = books["catalog"]["book"];
            console.log(books);
            res.render('booksjson', { title: 'Books JSON ', books:  books });
        }
    });

});


router.get('/rawdonutsxml', function(req, res, next) {
    var donutsxml = __dirname + "/../public/xml/donuts.xml";
    fs.readFile(donutsxml, "utf-8", function (error, text) {
        if (error) 
            throw error;
        else 
            res.render('raw', { title: 'Raw Donuts XML ', content: text });
    });
});


router.get('/rawdonutsjson', function(req, res, next) {
    var donutsjson = __dirname + "/../public/json/donuts.json";
    fs.readFile(donutsjson, "utf-8", function (error, text) {
        if (error) 
            throw error;
        else 
            res.render('raw', { title: 'Raw Donuts JSON ', content: text });
    });
});


router.get('/donutsxml', function(req, res, next) {
    var donutsxml = __dirname + "/../public/xml/donuts.xml";
    fs.readFile(donutsxml, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {
            parser.parseString(text, function (err, result) {
                var donuts = result['items']['item'];
                console.log(donuts[0]);
                res.render('donutsxml', { title: 'Donuts XML ', donuts:  donuts });
            });
        }
    });
});


router.get('/donutsjson', function(req, res, next) {
    var jsonFile = __dirname + "/../public/json/donuts.json";
    fs.readFile(jsonFile, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {
            var donuts = JSON.parse(text);
            donuts = donuts["items"]["item"];
            console.log(donuts);
            res.render('donutsjson', { title: 'Donuts JSON ', donuts:  donuts });
        }
    });

});
module.exports = router;
